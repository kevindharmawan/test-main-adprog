package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class IcFundsTest {

    private CalculatorInputHandler currentState;

    @BeforeEach
    void setup() {
        currentState = new IcFunds(new CalculatorMonthlyStart());
    }

    @Test
    void testResponseCorrect() {
        ResponseTemplate nextState = currentState.verificationMessage("100");
        Assertions.assertTrue(nextState instanceof IcTime);
    }

    @Test
    void testResponseCancel() {
        ResponseTemplate nextState = currentState.verificationMessage("batal");
        Assertions.assertNull(nextState);
    }
}
