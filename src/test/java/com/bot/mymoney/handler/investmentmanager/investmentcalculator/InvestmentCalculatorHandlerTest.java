package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InvestmentCalculatorHandlerTest {

    private InvestmentCalculatorHandler currentState;

    @BeforeEach
    void setup() {
        currentState = new InvestmentCalculatorHandler();
    }

//    @Test
//    void testResponseAnnually() {
//        ResponseTemplate nextState = currentState.verificationMessage("tahunan");
//        Assertions.assertTrue(nextState instanceof CalculatorAnnuallyStart);
//    }
//
//    @Test
//    void testResponseMonthly() {
//        ResponseTemplate nextState = currentState.verificationMessage("bulanan");
//        Assertions.assertTrue(nextState instanceof CalculatorMonthlyStart);
//    }

    @Test
    void testResponseCancel() {
        ResponseTemplate nextState = currentState.verificationMessage("batal");
        Assertions.assertNull(nextState);
    }
}
