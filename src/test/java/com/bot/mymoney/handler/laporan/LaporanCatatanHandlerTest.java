package com.bot.mymoney.handler.laporan;

import com.bot.mymoney.handler.laporan.LaporanWaktuHandler;
import com.bot.mymoney.handler.laporan.LaporanCatatanHandler;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class LaporanCatatanHandlerTest {
  private LaporanCatatanHandler currentState;

  @BeforeEach
  void setup() {
    currentState = new LaporanCatatanHandler("userid", "Anan");
  }

  @Test
  void testChooseCategory() {
    LaporanHandler nextState = currentState.verificationMessage("laporan pengeluaran");
    Assert.assertTrue(nextState instanceof LaporanWaktuHandler);
  }

  @Test
  void testChooseWrongCategory() {
    LaporanHandler nextState = currentState.verificationMessage("Intentional wrong input");
    Assert.assertTrue(nextState instanceof LaporanCatatanHandler);
  }

  @Test
  void testCancelOperation() {
    LaporanHandler nextState = currentState.verificationMessage("batal");
    Assert.assertTrue(nextState == null);
  }

  @Test
  void testGetDescription() {
    Assert.assertEquals("userid;Anan", currentState.getDescription());
  }
}

