package com.bot.mymoney.handler.record;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PendapatanNominalHandlerTest {
  PendapatanHandler currentHandler= new PendapatanNominalHandler(
      new PendapatanKategoriHandler("senderId", "name")
  );

  @Test
  void verificationMessageTrueTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("100000");
    assertTrue(nextHandler instanceof PendapatanKonfirmasiHandler);
  }

  @Test
  void verificationMessageBatalTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("batal");
    assertEquals(nextHandler, null);
    PendapatanTemplate handler = (PendapatanTemplate) currentHandler;
    Assert.assertEquals("Proses fitur dibatalkan", handler.getMessageToUser());
  }

  @Test
  void verificationMessageUnknownTest() {
    PendapatanTemplate nextHandler = this.currentHandler.verificationMessage("hai");
    assertTrue(nextHandler instanceof PendapatanNominalHandler);
  }

  @Test
  void getDescription() {
    String description = currentHandler.getDescription();
    assertEquals("senderId;name;null", description);
  }
}
