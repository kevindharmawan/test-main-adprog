package com.bot.mymoney.handler.expense;

import com.bot.mymoney.handler.ResponseTemplate;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PengeluaranKonfirmasiHandlerTest {

    private PengeluaranKonfirmasiHandler currentState;

    @BeforeEach
    void setup() {
        PengeluaranKategoriHandler firstState = new PengeluaranKategoriHandler("userid", "username");
        PengeluaranNominalHandler secondState = (PengeluaranNominalHandler) firstState.verificationMessage("konsumsi");
        currentState = (PengeluaranKonfirmasiHandler) secondState.verificationMessage("10000");
    }

    @Test
    void testKonfirmasiBerhasil() {
        PengeluaranHandler nextState = currentState.verificationMessage("ya");
        Assert.assertTrue(nextState == null);
    }

    @Test
    void testInputNonvalidNominal() {
        PengeluaranHandler nextState = currentState.verificationMessage("mantap");
        Assert.assertTrue(nextState instanceof PengeluaranKonfirmasiHandler);
    }

    @Test
    void testCancelOperation() {
        PengeluaranHandler nextState = currentState.verificationMessage("batal");
        Assert.assertTrue(nextState == null);
        Assert.assertEquals("Proses fitur dibatalkan", currentState.getMessageToUser());
    }

    @Test
    void testGetDescription() {
        Assert.assertEquals("userid;username;konsumsi;10000", currentState.getDescription());
    }
}
