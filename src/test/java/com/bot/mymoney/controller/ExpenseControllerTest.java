package com.bot.mymoney.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bot.mymoney.model.Expense;
import com.bot.mymoney.service.expense.ExpenseServiceImp;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

@WebMvcTest(controllers = ExpenseController.class)
public class ExpenseControllerTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private ExpenseServiceImp expenseService;

  private Expense expense;

  @BeforeEach
  public void setup() {
    expense = new Expense(
        "userid",
        "name",
        "category",
        "timestamp",
        "nominal"
    );
  }

  @Test
  public void testControllerGetAll() throws Exception {
    List<Expense> list = new ArrayList<>();
    list.add(expense);
    when(expenseService.getAll()).thenReturn(list);

    mvc.perform(get("/expense/get").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].category").value("category"));
  }

  @Test
  public void testControllerGetByUserId() throws Exception {
    List<Expense> list = new ArrayList<>();
    list.add(expense);
    when(expenseService.getByUserId("userid")).thenReturn(list);

    mvc.perform(get("/expense/getbyuserid/userid").contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].category").value("category"));
  }

  @Test
  public void testControllerProcess() throws Exception {
    JSONObject json = new JSONObject();
    json.put("senderId", "userid");
    json.put("username", "username");
    json.put("message", "message");
    when(expenseService.handle("userid", "username", "message")).thenReturn("accepted");

    mvc.perform(post("/expense")
        .contentType(MediaType.APPLICATION_JSON).content(json.toString()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$").value("accepted"));
  }
}
