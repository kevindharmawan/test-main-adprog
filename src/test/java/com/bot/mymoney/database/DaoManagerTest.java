package com.bot.mymoney.database;

import com.linecorp.bot.client.LineMessagingClient;
import com.linecorp.bot.client.LineSignatureValidator;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DaoManagerTest {
    @Mock
    private Environment env;

    @InjectMocks
    private DaoManager daoManager;

    @Test
    void getChannelSecretTest() {
        when(env.getProperty("com.linecorp.channel_secret")).thenReturn("channel secret");
        daoManager.getChannelSecret();
        verify(env).getProperty("com.linecorp.channel_secret");
    }

    @Test
    void getChannelAccessTokenTest() {
        when(env.getProperty("com.linecorp.channel_access_token")).thenReturn("channel access token");
        daoManager.getChannelAccessToken();
        verify(env).getProperty("com.linecorp.channel_access_token");
    }

//    @Test
//    void getMessagingClientTest() {
//        when(env.getProperty("com.linecorp.channel_access_token")).thenReturn("channel access token");
//        daoManager.getMessagingClient();
//        verify(env).getProperty("com.linecorp.channel_access_token");
//    }

    @Test
    void getSignatureValidatorTest() {
        when(env.getProperty("com.linecorp.channel_secret")).thenReturn("channel secret");
        LineSignatureValidator lineSignatureValidator = daoManager.getSignatureValidator();
        Assert.assertNotNull(lineSignatureValidator);
    }

    @Test
    void getDataSourceTest() {
        DataSource source = daoManager.getDataSource();
        Assert.assertNotNull(source);
    }

    @Test
    void getUserDao() {
        UserDao userDao = daoManager.getUserDao();
        Assert.assertNotNull(userDao);
    }

    @Test
    void getSpendingDao() {
        RecordDao spendingDao = daoManager.getRecordDao();
        Assert.assertNotNull(spendingDao);
    }
}
