package com.bot.mymoney.database;

import com.bot.mymoney.model.Record;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.sql.DataSource;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class RecordDaoTest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private ResultSet resultSet;

    @InjectMocks
    RecordDaoImp recordDao = new RecordDaoImp(getDataSource());

    DataSource getDataSource() {
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        String username = System.getenv("JDBC_DATABASE_USERNAME");
        String password = System.getenv("JDBC_DATABASE_PASSWORD");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Test
    void getTest() {
        List<Record> records = recordDao.get();
        Assert.assertNull(records);
    }

    @Test
    void getByUserIdTest() {
        List<Record> records = recordDao.getByUserId("userId");
        Assert.assertNull(records);
    }

    @Test
    void saveRecordTest() {
        int record = recordDao.saveRecord("userId", "name", "category",
                "timestamp", "nominal");
        Assert.assertEquals(record, 0);
    }

    @Test
    void extractDataTest() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        List<Record> records = recordDao.extractData(resultSet);
        Assert.assertNotNull(records);
    }
}