package com.bot.mymoney.database;

import com.bot.mymoney.model.Expense;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ExpenseDaoTest {
    @Mock
    private JdbcTemplate jdbcTemplate;

    @Mock
    private ResultSet resultSet;

    @InjectMocks
    ExpenseDaoImpl expenseDao = new ExpenseDaoImpl(getDataSource());

    DataSource getDataSource() {
        String dbUrl = System.getenv("JDBC_DATABASE_URL");
        String username = System.getenv("JDBC_DATABASE_USERNAME");
        String password = System.getenv("JDBC_DATABASE_PASSWORD");
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        dataSource.setDriverClassName("org.postgresql.Driver");
        dataSource.setUrl(dbUrl);
        dataSource.setUsername(username);
        dataSource.setPassword(password);

        return dataSource;
    }

    @Test
    void getTest() {
        List<Expense> expenses = expenseDao.get();
        Assert.assertNull(expenses);
    }

    @Test
    void getByUserIdTest() {
        List<Expense> expenses = expenseDao.getByUserId("userId");
        Assert.assertNull(expenses);
    }

    @Test
    void saveRecordTest() {
        int status = expenseDao.saveExpense("userId", "name", "category",
            "timestamp", "nominal");
        Assert.assertEquals(status, 0);
    }

    @Test
    void extractDataTest() throws SQLException {
        when(resultSet.next()).thenReturn(true).thenReturn(false);
        List<Expense> expenses = expenseDao.extractData(resultSet);
        Assert.assertNotNull(expenses);
    }
}
