package com.bot.mymoney.service.expense;

import static org.mockito.Mockito.when;

import com.bot.mymoney.model.Expense;
import com.bot.mymoney.repository.ExpenseRepository;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(MockitoExtension.class)
public class ExpenseServiceTest {

  @Mock
  ExpenseRepository expenseRepository;

  @InjectMocks
  ExpenseServiceImp expenseService;

  private Expense expense;

  @BeforeEach
  void setup() throws Exception {
    expense = new Expense(
        "userid",
        "name",
        "category",
        "timestamp",
        "10000"
    );
  }

  @Test
  public void testExpenseServiceGetAll() {
    when(expenseRepository.getAll()).thenReturn(new ArrayList<>());

    Assert.assertTrue(expenseService.getAll() instanceof ArrayList);
  }

  @Test
  public void testExpenseServiceGetByUserId() {
    List<Expense> list = new ArrayList<>();
    list.add(expense);
    when(expenseRepository.getByUserId("userid")).thenReturn(list);

    Assert.assertTrue(expenseService.getByUserId("userid").contains(expense));
  }

  @Test
  public void testExpenseServiceHandle() {
    Assert.assertTrue(
        expenseService.handle("userid", "username", "konsumsi")
            .contains("konsumsi"));
    Assert.assertTrue(
        expenseService.handle("userid", "username", "20000")
            .contains("20000"));
    Assert.assertTrue(
        expenseService.handle("userid", "username", "ya")
            .contains("berhasil dicatat"));
  }
}
