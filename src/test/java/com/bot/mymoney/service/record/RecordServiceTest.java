package com.bot.mymoney.service.record;

import static org.mockito.Mockito.when;
import com.bot.mymoney.model.Record;
import com.bot.mymoney.repository.RecordRepository;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class RecordServiceTest {

  @InjectMocks
  RecordServiceImp recordService;

  @Mock
  RecordRepository recordRepository;

  private Record record;

  @BeforeEach
  void setup() throws Exception {
    record = new Record(
        "userid",
        "name",
        "category",
        "timestamp",
        "10000"
    );
  }

  @Test
  public void testExpenseServiceGetAll() {
    when(recordRepository.getAll()).thenReturn(new ArrayList<>());
    Assert.assertTrue(recordService.getAll() instanceof ArrayList);
  }

  @Test
  public void testExpenseServiceGetByUserId() {
    List<Record> list = new ArrayList<>();
    list.add(record);
    when(recordRepository.getByUserId("userid")).thenReturn(list);
    Assert.assertTrue(recordService.getByUserId("userid").contains(record));
  }

  @Test
  public void testExpenseServiceHandle() {
    Assert.assertTrue(
        recordService.handle("userid", "username", "gaji")
            .contains("gaji"));
    Assert.assertTrue(
        recordService.handle("userid", "username", "50000")
            .contains("50000"));
    Assert.assertTrue(
        recordService.handle("userid", "username", "ya")
            .contains("berhasil dicatat"));
  }
}
