package com.bot.mymoney.controller;

import com.bot.mymoney.model.Record;
import com.bot.mymoney.service.record.RecordService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/** Controller for expense API access. **/
@RestController
@RequestMapping("/record")
public class RecordController {

  @Autowired
  RecordService recordService;

  @GetMapping(path = "/get", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity<Iterable<Record>> get() {
    return ResponseEntity.ok(recordService.getAll());
  }

  @GetMapping(path = "/getbyuserid/{userid}", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity<Iterable<Record>> getByUserId(
      @PathVariable(value = "userid") String userId) {
    return ResponseEntity.ok(recordService.getByUserId(userId));
  }

  @PostMapping(path = "", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity process(@RequestBody Map<String, String> json) {
    return ResponseEntity.ok(recordService.handle(json.get("senderId"),
        json.get("username"), json.get("message")));
  }
}
