package com.bot.mymoney.controller;

import com.bot.mymoney.service.laporan.LaporanService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/** Controller for Laporan API access. **/
@RestController
@RequestMapping("/report")
public class LaporanController {
  @Autowired
  LaporanService laporanService;

  @PostMapping(path = "", produces = {"application/json"})
  @ResponseBody
  public ResponseEntity process(@RequestBody Map<String, String> json) {
    return ResponseEntity.ok(laporanService
        .handle(json.get("senderId"), json.get("username"), json.get("message")));
  }

}
