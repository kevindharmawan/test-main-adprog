package com.bot.mymoney.database;

import com.bot.mymoney.model.Expense;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

/**
 * Implementation of ExpenseDaoImpl.
 **/
public class ExpenseDaoImpl implements ExpenseDao {
  private JdbcTemplate jdbcTemplate;
  private static final String EXPENSE_TABLE = "tbl_expense";
  private static final String SQL_SELECT_ALL = "SELECT * FROM " + EXPENSE_TABLE;
  private static final ResultSetExtractor<List<Expense>>
      EXPENSE_EXTRACTOR = ExpenseDaoImpl::extractData;

  public ExpenseDaoImpl(DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
  }

  /**
   * Extract data from ResultSet to List of Expenses.
   **/
  public static List<Expense> extractData(ResultSet resultSet) throws SQLException {
    List<Expense> extractList = new ArrayList<Expense>();
    while (resultSet.next()) {
      Expense expense = new Expense(
          resultSet.getString("user_id"),
          resultSet.getString("name"),
          resultSet.getString("category"),
          resultSet.getString("timestamp"),
          resultSet.getString("nominal"));
      extractList.add(expense);
    }
    return extractList;
  }

  @Override
  public List<Expense> get() {
    return jdbcTemplate.query(SQL_SELECT_ALL, EXPENSE_EXTRACTOR);
  }

  @Override
  public List<Expense> getByUserId(String userId) {
    String sqlGetByUserId = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
    return jdbcTemplate.query(sqlGetByUserId,
      new Object[]{"%" + userId + "%"},
      EXPENSE_EXTRACTOR);
  }

  @Override
  public int saveExpense(String userId, String name, String category,
                         String timestamp, String nominal) {
    String register = "INSERT INTO " + EXPENSE_TABLE
        + " (user_id, name, category, timestamp, nominal) VALUES (?, ?, ?, ?, ?);";
    return jdbcTemplate.update(register, userId, name, category, timestamp, nominal);
  }
}
