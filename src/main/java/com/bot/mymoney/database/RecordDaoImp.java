package com.bot.mymoney.database;

import com.bot.mymoney.model.Record;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

/** RecordDaoImp. **/
public class RecordDaoImp implements RecordDao {
  private static final String RECORD_TABLE = "tbl_record";
  private static final String SQL_SELECT_ALL = "SELECT * FROM " + RECORD_TABLE;
  private static final ResultSetExtractor<List<Record>>
      RECORD_EXTRACTOR = RecordDaoImp::extractData;
  private JdbcTemplate jdbcTemplate;

  public RecordDaoImp(DataSource dataSource) {
    jdbcTemplate = new JdbcTemplate(dataSource);
  }

  /**   extract data for record table.   **/
  public static List<Record> extractData(ResultSet resultSet) throws SQLException {
    List<Record> extractList = new ArrayList<Record>();
    while (resultSet.next()) {
      Record sp = new Record(
          resultSet.getString("user_id"),
          resultSet.getString("name"),
          resultSet.getString("category"),
          resultSet.getString("timestamp"),
          resultSet.getString("nominal"));
      extractList.add(sp);
    }
    return extractList;
  }

  /**   function for record database.   **/
  @Override
  public List<Record> get() {
    return jdbcTemplate.query(SQL_SELECT_ALL, RECORD_EXTRACTOR);
  }

  @Override
  public List<Record> getByUserId(String userId) {
    String sqlGetByUserId = SQL_SELECT_ALL + " WHERE LOWER(user_id) LIKE LOWER(?);";
    return jdbcTemplate.query(sqlGetByUserId,
        new Object[]{"%" + userId + "%"},
        RECORD_EXTRACTOR);
  }

  @Override
  public int saveRecord(String userId, String name, String category,
                        String timestamp, String nominal) {
    String register = "INSERT INTO " + RECORD_TABLE
        + " (user_id, name, category, timestamp, nominal) VALUES (?, ?, ?, ?, ?);";
    return jdbcTemplate.update(register, userId, name, category, timestamp, nominal);
  }
}
