package com.bot.mymoney.model;

/**
 * Expense model.
 **/
public class Expense {
  private String userId;
  private String name;
  private String category;
  private String timestamp;
  private String nominal;

  /** Expense model constructor. **/
  public Expense(String userId, String name, String category, String timestamp, String nominal) {
    this.userId = userId;
    this.name = name;
    this.category = category;
    this.timestamp = timestamp;
    this.nominal = nominal;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserId() {
    return this.userId;
  }

  public void setName(String title) {
    this.name = title;
  }

  public String getName() {
    return this.name;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public String getCategory() {
    return this.category;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  public String getTimestamp() {
    return this.timestamp;
  }

  public void setNominal(String nominal) {
    this.nominal = nominal;
  }

  public String getNominal() {
    return this.nominal;
  }
}
