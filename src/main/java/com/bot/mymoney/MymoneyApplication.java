package com.bot.mymoney;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

/** MyMoneyApplication. **/
@SpringBootApplication
public class MymoneyApplication {

  public static void main(String[] args) {
    SpringApplication.run(MymoneyApplication.class, args);
  }

}
