package com.bot.mymoney.handler.laporan;

import java.util.Arrays;
import java.util.List;

/** Implementasi laporan catatan untuk memilih catatan. **/
public class LaporanCatatanHandler extends LaporanHandler {

  public static final List<String> kategoriCatatan = Arrays.asList(
      "laporan pengeluaran",
      "laporan pendapatan"
  );

  public LaporanCatatanHandler(String userId, String name) {
    this.description = userId + ";" + name;
  }

  @Override
  public LaporanHandler verificationMessage(String message) {
    if (kategoriCatatan.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public LaporanHandler handle(String message) {
    description += ";" + message;
    messageToUser = "Berapa jangka waktu laporan " + message
      + " yang ingin kamu pilih? Jawab dengan 'harian', 'mingguan', atau 'bulanan'.";
    return new LaporanWaktuHandler(this);
  }

  @Override
  public LaporanHandler unknownMessage() {
    messageToUser = "Kategori yang dipilih tidak ada, tolong pilih salah satu dari "
        + "kategori yang tersedia. Ketik 'Batal' jika ingin membatalkan.";
    return this;
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}
