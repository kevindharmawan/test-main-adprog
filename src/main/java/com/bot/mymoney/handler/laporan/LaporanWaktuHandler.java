package com.bot.mymoney.handler.laporan;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.bot.mymoney.model.Expense;
import org.springframework.web.client.RestTemplate;

/** LaporanWaktuHandler untuk memilih waktu dan mengeluarkan output nominal berdasarkan waktu. **/
public class LaporanWaktuHandler extends LaporanHandler {
  public static final List<String> kategoriWaktu = Arrays.asList("harian", "mingguan", "bulanan");
  final String url = "https://testing-adprog.herokuapp.com/";
  LaporanCatatanHandler catatan;
  RestTemplate restTemplate;

  /** LaporanWaktuHandler constructor sebagai catatan untuk catatanhandler. **/
  public LaporanWaktuHandler(LaporanCatatanHandler catatan) {
    this.catatan = catatan;
    this.description = "";
    restTemplate = new RestTemplate();
  }

  @Override
  public LaporanHandler verificationMessage(String message) {
    if (kategoriWaktu.contains(message)) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public LaporanHandler handle(String message) {
    description = catatan.getDescription() +  ";" + message;
    int days = this.stringToDay(message);
    String catat = getDescription().split(";") [2];
    messageToUser = categoryCatatan(catat, days);
    return null;
  }

  @Override
  public LaporanHandler unknownMessage() {
    messageToUser = "Kategori yang dipilih tidak ada, tolong pilih salah satu dari "
            + "kategori yang tersedia. Ketik 'Batal' jika ingin membatalkan.";
    return this;
  }

  @Override
  public String getDescription() {
    return catatan.getDescription();
  }

  private String categoryCatatan(String catat, int days) {
    if (catat.equalsIgnoreCase("laporan pengeluaran")) {
      return totalNominalExpense(days);
    }
    return totalNominalRecords(days);
  }

  private String totalNominalExpense(int hari) {
    int totalNominal = 0;
    String userId = getDescription().split(";") [0];
    LocalDateTime dateNow = LocalDateTime.now().plusHours(7);

    List<Expense> expenses = extractExpense(userId, dateNow, hari);

    for (Expense expense : expenses) {
      totalNominal += Integer.parseInt(expense.getNominal());
    }

    return "Jumlah Pengeluaran Total = " + totalNominal;
  }

  /**
   * Menerima id user, waktu, dan batas durasi dalam hari dari waktu.
   * Mengembalikan List<Expense> hasil ekstraksi.
   */
  public List<Expense> extractExpense(String userId, LocalDateTime time, int hari) {
    List<Expense> expenses = new ArrayList<>();
    String response = restTemplate.getForObject(url +
        "expense/getbyuserid/" + userId, String.class);
    response = response.substring(1, response.length() - 1)
        .replace("}", "")
        .replace("{", "")
        .replace("\"", "")
        .replace("userId:", "")
        .replace("name:", "")
        .replace("category:", "")
        .replace("timestamp:", "")
        .replace("nominal:", "");

    String[] dataAsArray = response.split(",");

    for (int i = 0; i < dataAsArray.length / 5; i++) {
      LocalDateTime timestamp = LocalDateTime.parse(dataAsArray[i * 5 + 3]);
      if (Duration.between(timestamp, time).toDays() < hari) {
        expenses.add(new Expense(
            dataAsArray[i * 5 + 0],
            dataAsArray[i * 5 + 1],
            dataAsArray[i * 5 + 2],
            dataAsArray[i * 5 + 3],
            dataAsArray[i * 5 + 4]
            ));
      }
    }

    return expenses;
  }

  private String totalNominalRecords(int hari) {
    int totalNominal = 0;
    //    String userId = getDescription().split(";") [0];
    //    List<Record> listOfRecord = recordDb.getByUserId(userId);
    //    LocalDateTime dateNow = LocalDateTime.now().plusHours(7);
    //    for (Record record : listOfRecord) {
    //      LocalDateTime date = LocalDateTime.parse(record.getTimestamp(),
    //          DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm"));
    //      if (Duration.between(dateNow, date).toDays() < hari) {
    //        totalNominal += Integer.valueOf(record.getNominal());
    //      }
    //    }
    return "Jumlah Pendapatan Total = " + totalNominal;
  }

  private int stringToDay(String message) {
    if (message.equalsIgnoreCase("Harian")) {
      return 1;
    } else if (message.equalsIgnoreCase("Mingguan")) {
      return 7;
    } else {
      return 30;
    }
  }

}

