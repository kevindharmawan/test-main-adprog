package com.bot.mymoney.handler.laporan;

import com.bot.mymoney.handler.Handler;
import com.bot.mymoney.handler.ResponseTemplate;

/** abstract class for laporan handler. **/
public abstract class LaporanHandler {
  protected LaporanHandler handler;
  protected String description;
  protected String messageToUser;

  /** 'else' for verificationMessage. **/
  public LaporanHandler cancelOperation(String message) {
    if (message.equalsIgnoreCase("batal")) {
      messageToUser = "Proses fitur dibatalkan";
      return null;
    }
    return unknownMessage();
  }

  public String getMessageToUser() {
    return messageToUser;
  }

  public abstract LaporanHandler unknownMessage();

  public abstract LaporanHandler verificationMessage(String message);

  public abstract LaporanHandler handle(String message);

  public abstract String getDescription();
}
