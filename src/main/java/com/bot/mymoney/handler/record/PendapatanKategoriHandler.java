package com.bot.mymoney.handler.record;

import java.util.Arrays;
import java.util.List;

/** KategoriHandler. **/
public class PendapatanKategoriHandler extends PendapatanTemplate implements PendapatanHandler {
  private final List<String> categories = Arrays.asList("gaji", "usaha", "lainnya");

  public PendapatanKategoriHandler(String senderId, String name) {
    this.description = senderId + ";" + name;
  }

  @Override
  public PendapatanTemplate verificationMessage(String message) {
    if (categories.contains(message)) {
      return handle(message);
    }
    return cancelOperation(message);
  }

  @Override
  public PendapatanTemplate handle(String message) {
    description += ";" + message;
    messageToUser = "Kategori " + message + " berhasil dipilih. "
        + "Berapa pendapatan kamu? \nContoh: 8000000";
    return new PendapatanNominalHandler(this);
  }

  @Override
  public PendapatanTemplate unknownMessage() {
    messageToUser = "Tidak terdapat kategori tersebut! "
        + "Silakan pilih kembali kategori yang tersedia "
        + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }

  @Override
  public String getDescription() {
    return this.description;
  }
}
