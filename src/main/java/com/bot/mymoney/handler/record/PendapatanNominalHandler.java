package com.bot.mymoney.handler.record;

/** NominalHandler. **/
public class PendapatanNominalHandler extends PendapatanTemplate implements PendapatanHandler {
  public PendapatanNominalHandler(PendapatanKategoriHandler handler) {
    this.handler = handler;
  }

  @Override
  public PendapatanTemplate verificationMessage(String message) {
    try {
      return handle(message);
    } catch (NumberFormatException e) {
      return cancelOperation(message);
    }
  }

  @Override
  public PendapatanTemplate handle(String message) {
    Integer.parseInt(message);
    description = message;
    messageToUser = "Apakah benar pendapatan kamu sebesar Rp" + message
        + "? Konfirmasi pencatatan dengan menjawab 'Ya' "
        + "atau ketik 'Batal' untuk membatalkan tindakan";
    return new PendapatanKonfirmasiHandler(this);
  }

  @Override
  public PendapatanTemplate unknownMessage() {
    messageToUser = "Nominal uang tidak sesuai. "
        + "Pastikan kamu hanya memasukkan angka, contoh: 50000. "
        + "Jika ingin membatalkan tindakan, ketik 'Batal'";
    return this;
  }

  @Override
  public String getDescription() {
    return handler.getDescription() + ";" + description;
  }
}
