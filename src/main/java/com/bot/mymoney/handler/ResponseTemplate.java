package com.bot.mymoney.handler;

/** abstract class for every nextHandler. **/
public abstract class ResponseTemplate {
  protected Handler state;
  protected String description;
  protected String messageToUser;

  public abstract ResponseTemplate unknownMessage();

  /** 'else' for verificationMessage. **/
  public ResponseTemplate cancelOperation(String message) {
    if (message.equalsIgnoreCase("batal")) {
      messageToUser = "Proses fitur dibatalkan";
      return null;
    }
    return unknownMessage();
  }

  public String getMessageToUser() {
    return messageToUser;
  }
}
