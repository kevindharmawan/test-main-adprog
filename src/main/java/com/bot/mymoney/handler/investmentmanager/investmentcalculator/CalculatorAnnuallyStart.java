package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;

/** CalculatorAnnuallyStart. **/
public class CalculatorAnnuallyStart extends InvestmentCalculator {
  public CalculatorAnnuallyStart() {
    mode = "Awal Tahun";
  }

  @Override
  public ResponseTemplate handle(String message) {
    messageToUser = "Berapa saldo awal kamu? Contoh: 100000";
    return new IcFunds(this);
  }

  @Override
  protected void calculateInvestment() {
    contrib = vars.get("contrib") * vars.get("time");
    result = getInvestmentGrowth();
  }

  @Override
  public String response() {
    calculateInvestment();
    return "Dalam jangka waktu " + vars.get("time")
        + " tahun, saldo akhirmu akan menjadi Rp" + result
        + " dengan total kontribusi Rp" + contrib + ".";
  }
}
