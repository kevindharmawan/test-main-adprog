package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;

/** ICConfirm. **/
public class IcConfirm extends CalculatorInputHandler {
  public IcConfirm(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    if ("ya".equalsIgnoreCase(message)) {
      description = message;
      messageToUser = calculator.response();
      return null;
    } else {
      throw new NumberFormatException();
    }
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
