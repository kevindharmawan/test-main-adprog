package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;

/** ICTime. **/
public class IcTime extends CalculatorInputHandler {
  public IcTime(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("time", value);
    //TODO: Message
    return new IcRate(calculator);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
