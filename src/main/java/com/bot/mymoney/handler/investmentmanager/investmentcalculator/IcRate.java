package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;

/** ICRate. **/
public class IcRate extends CalculatorInputHandler {
  public IcRate(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("rate", value);
    //TODO: Message
    return new IcContrib(calculator);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
