package com.bot.mymoney.handler.investmentmanager.investmentcalculator;

import com.bot.mymoney.handler.ResponseTemplate;

/** ICContrib. **/
public class IcContrib extends CalculatorInputHandler {
  public IcContrib(InvestmentCalculator calculator) {
    super(calculator);
  }

  @Override
  public ResponseTemplate handle(String message) {
    Integer value = Integer.valueOf(message);
    calculator.input("rate", value);
    description = message;
    messageToUser = "Apakah benar input anda:"
        + calculator.getValues() + "? Ketik 'ya' untuk lanjut.";
    return new IcConfirm(calculator);
  }

  @Override
  public String getDescription() {
    //TODO
    return null;
  }
}
