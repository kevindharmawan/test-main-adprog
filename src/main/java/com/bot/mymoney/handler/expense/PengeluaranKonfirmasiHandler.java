package com.bot.mymoney.handler.expense;

/**
 * Handler untuk konfirmasi pencatatan catat pengeluaran.
 **/
public class PengeluaranKonfirmasiHandler extends PengeluaranHandler {
  public PengeluaranKonfirmasiHandler(PengeluaranNominalHandler handler) {
    this.handler = handler;
    this.description = "";
  }

  @Override
  public PengeluaranHandler unknownMessage() {
    messageToUser = "Untuk konfirmasi pencatatan jawab 'Ya'"
        + " dan untuk pembatalan pencatatan jawab 'Batal'.";
    return this;
  }

  @Override
  public PengeluaranHandler verificationMessage(String message) {
    if (message.equalsIgnoreCase("ya")) {
      return handle(message);
    } else {
      return cancelOperation(message);
    }
  }

  @Override
  public PengeluaranHandler handle(String message) {
    messageToUser = "Pengeluaran kamu berhasil dicatat!";
    description = "";
    return null;
  }

  @Override
  public String getDescription() {
    return handler.getDescription();
  }
}
