package com.bot.mymoney.service.laporan;

import com.bot.mymoney.model.Expense;
import java.util.List;

/** Service for ExpenseController. **/
public interface LaporanService {

  String handle(String userId, String username, String message);
}
