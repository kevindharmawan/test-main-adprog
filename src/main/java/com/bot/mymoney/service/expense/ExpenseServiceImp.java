package com.bot.mymoney.service.expense;

import com.bot.mymoney.handler.expense.PengeluaranHandler;
import com.bot.mymoney.handler.expense.PengeluaranKategoriHandler;
import com.bot.mymoney.model.Expense;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.bot.mymoney.repository.ExpenseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/** ExpenseService Implementation. **/
@Service
public class ExpenseServiceImp implements ExpenseService {

  @Autowired
  private ExpenseRepository expenseRepository;

  private HashMap<String, PengeluaranHandler> currentHandler = new HashMap<>();

  public List<Expense> getAll() {
    return expenseRepository.getAll();
  }

  public List<Expense> getByUserId(String userId) {
    return expenseRepository.getByUserId(userId);
  }

  @Override
  public String handle(String userId, String username, String message) {
    if (!currentHandler.keySet().contains(userId) || currentHandler.get(userId) == null) {
      currentHandler.put(userId, new PengeluaranKategoriHandler(userId, username));
    }

    PengeluaranHandler handler = currentHandler.get(userId);
    currentHandler.put(userId, handler.verificationMessage(message));

    if (handler.getMessageToUser().equals("Pengeluaran kamu berhasil dicatat!")) {
      expenseRepository.saveExpense(handler.getDescription());
    }

    return handler.getMessageToUser();
  }
}
