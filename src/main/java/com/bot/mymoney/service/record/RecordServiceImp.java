package com.bot.mymoney.service.record;

import com.bot.mymoney.handler.record.PendapatanHandler;
import com.bot.mymoney.handler.record.PendapatanKategoriHandler;
import com.bot.mymoney.handler.record.PendapatanTemplate;
import com.bot.mymoney.model.Record;
import com.bot.mymoney.repository.RecordRepository;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** RecordService Implementation. **/
@Service
public class RecordServiceImp implements RecordService {

  @Autowired
  private RecordRepository recordRepository;

  private HashMap<String, PendapatanTemplate> currentHandler = new HashMap<>();

  public List<Record> getAll() {
    return recordRepository.getAll();
  }

  public List<Record> getByUserId(String userId) {
    return recordRepository.getByUserId(userId);
  }

  @Override
  public String handle(String userId, String username, String message) {
    Set<String> keySet = currentHandler.keySet();
    if (!keySet.contains(userId) || currentHandler.get(userId) == null) {
      currentHandler.put(userId, new PendapatanKategoriHandler(userId, username));
    }

    PendapatanTemplate current = currentHandler.get(userId);
    PendapatanHandler handler = (PendapatanHandler) current;
    currentHandler.put(userId, handler.verificationMessage(message));

    if (current.getMessageToUser().equals("Pendapatan kamu berhasil dicatat!")) {
      recordRepository.saveRecord(handler.getDescription());
    }

    return current.getMessageToUser();
  }
}
