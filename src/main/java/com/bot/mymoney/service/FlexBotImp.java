package com.bot.mymoney.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.linecorp.bot.model.message.FlexMessage;
import com.linecorp.bot.model.message.flex.container.FlexContainer;
import com.linecorp.bot.model.objectmapper.ModelObjectMapper;
import java.util.Objects;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Service;

/** FlexBotImplemetation. **/
@Service
public class FlexBotImp implements FlexBot {

  /** untuk gambar pilihan perintah. **/
  public FlexMessage createFlexMenu() {
    FlexMessage flexMessage = new FlexMessage("Menu", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(
          classLoader.getResourceAsStream("menu.json"));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Menu", flexContainer);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }

  /** untuk gambar pilihan kategori pendapatan. **/
  public FlexMessage createFlexCatatPendapatan() {
    FlexMessage flexMessage = new FlexMessage("Kategori Pendapatan", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(
          classLoader.getResourceAsStream("catatPendapatan.json"));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Kategori Pendapatan", flexContainer);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }

  /** untuk gambar pilihan kategori pengeluaran. **/
  public FlexMessage createFlexCatatPengeluaran() {
    FlexMessage flexMessage = new FlexMessage("Kategori Pengeluaran", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(
          classLoader.getResourceAsStream("catatPengeluaran.json"));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Kategori Pengeluaran", flexContainer);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }

  /** untuk gambar pilihan laporan. **/
  public FlexMessage createFlexLaporan() {
    FlexMessage flexMessage = new FlexMessage("Kategori Laporan", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(Objects.requireNonNull(
          classLoader.getResourceAsStream("laporan.json")));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Kategori Laporan", flexContainer);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }

  /** untuk gambar pilihan waktu laporan. **/
  public FlexMessage createFlexLaporanWaktu() {
    FlexMessage flexMessage = new FlexMessage("Waktu Laporan", null);
    try {
      ClassLoader classLoader = getClass().getClassLoader();
      String flexTemplate = IOUtils.toString(Objects.requireNonNull(
          classLoader.getResourceAsStream("laporanWaktu.json")));

      ObjectMapper objectMapper = ModelObjectMapper.createNewObjectMapper();
      FlexContainer flexContainer = objectMapper.readValue(flexTemplate, FlexContainer.class);
      flexMessage = new FlexMessage("Waktu Laporan", flexContainer);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return flexMessage;
  }
}
